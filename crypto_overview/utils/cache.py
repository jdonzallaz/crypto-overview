import functools

import diskcache


def cache_method(base_key: str, expire: int = 3600, cache_attr: str = "cache", ignore_cache_arg: str = "ignore_cache"):
    """
    Decorator that encapsulate diskcache.Cache for a class instance method. First attribute of the decorated function
    must be "self".
    The class must have a "cache" attribute, configurable with cache_attr (accessed with self.{cache_attr}).
    The decorated method can have an argument to ignore the cache when calling the function. The argument must be
    keyword-only, configurable with ignore_cache_arg.

    :param base_key: the key to use in the cache
    :param expire: the expiration time in second
    :param cache_attr: the name of the attribute in the class that hold the diskcache.Cache instance
    :param ignore_cache_arg: the name of the argument of the method to ignore the cache
    :return: a decorator handling the cache for a method
    """

    def _decorator(function):
        @functools.wraps(function)
        def wrapper(self, *args, **kwargs):
            cache = getattr(self, cache_attr)

            key = diskcache.core.args_to_key(
                base=(base_key,), args=args, kwargs=kwargs, typed=False, ignore=set(ignore_cache_arg)
            )
            ignore_cache = ignore_cache_arg in kwargs and kwargs[ignore_cache_arg]
            if not ignore_cache:
                assets = cache.get(key)
                if assets:
                    return assets

            result = function(self, *args, **kwargs)

            cache.set(key, result, expire=expire)

            return result

        return wrapper

    return _decorator
