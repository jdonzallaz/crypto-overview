import logging
from pathlib import Path
from typing import Optional, Union

from colorlog import ColoredFormatter

LOG_FORMAT_CONSOLE = "%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
LOG_FORMAT_FILE = "%(asctime)s | %(levelname)-8s | %(name)-12s | %(message)s"


def init_log(file_path: Optional[Path] = None, log_level: Union[int, str] = logging.WARNING):
    """
    Init logging globally. Optionally enable file logging if a path to a file is given.
    The console logging use color-formatting according to the log level of the message.
    It configures the root logger, accessed with "logging.getLogger()".

    :param file_path: an optional path to a file to enable file-logging
    :param log_level: the initial logging level (default to warning level)
    """

    if isinstance(log_level, str):
        if log_level == "debug":
            log_level = logging.DEBUG
        elif log_level == "info":
            log_level = logging.INFO
        elif log_level == "warning":
            log_level = logging.WARNING
        elif log_level == "error":
            log_level = logging.ERROR
        elif log_level == "fatal":
            log_level = logging.FATAL
        else:
            log_level = logging.WARNING

    logging.root.setLevel(log_level)
    logger = logging.getLogger()

    # stream handler
    stream_handler = logging.StreamHandler()
    color_formatter = ColoredFormatter(LOG_FORMAT_CONSOLE)
    stream_handler.setFormatter(color_formatter)
    logger.addHandler(stream_handler)

    # file handler
    if file_path is not None:
        file_path.parent.mkdir(parents=True, exist_ok=True)
        file_handler = logging.FileHandler(file_path)
        file_formatter = logging.Formatter(LOG_FORMAT_FILE)
        file_handler.setFormatter(file_formatter)
        logger.addHandler(file_handler)


def set_log_level(level: int):
    """
    Set the logging level of the root logger.

    :param level: a logging level (e.g. logging.WARNING)
    """

    logger = logging.getLogger()
    logger.setLevel(level)
