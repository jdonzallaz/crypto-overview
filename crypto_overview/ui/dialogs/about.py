import logging
import platform
import sys
from pathlib import Path

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt

from crypto_overview import constants


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent):
        super().__init__(parent, Qt.WindowCloseButtonHint)
        self.setWindowTitle("About")

        # Buttons
        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.button_box.clicked.connect(self.reject)

        # Main layout
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.main_layout)

        # Infos
        name = constants.NAME
        version = constants.VERSION
        build_time = constants.BUILD_TIME
        commit = constants.COMMIT
        runtime = sys.version
        os_version = platform.platform()
        log_files = [Path(handler.baseFilename) for handler in logging.getLogger().handlers if
                     isinstance(handler, logging.FileHandler)]
        if len(log_files) > 0:
            log_file = f"[{log_files[0].as_posix()}]({log_files[0].as_uri()})"
        else:
            log_file = "None"
        author = f"{constants.AUTHOR_NAME} ({constants.AUTHOR_EMAIL})"
        website = constants.WEBSITE_LINK
        license = constants.LICENSE

        text = QtWidgets.QLabel(
            f"### {name} v{version}\n"
            f"Build time: {build_time}  \n"
            f"Commit: {commit}  \n"
            f"Runtime: {runtime}  \n"
            f"OS: {os_version}  \n"
            f"Log file: {log_file}  \n"
            f"Author: {author}  \n"
            f"Website: {website}  \n"
            f"License: {license}  \n"
        )
        text.setTextFormat(Qt.MarkdownText)
        text.setOpenExternalLinks(True)

        self.main_layout.addWidget(text)
        self.main_layout.addWidget(self.button_box)
