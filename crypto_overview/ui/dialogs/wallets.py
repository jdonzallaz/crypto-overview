import logging
from dataclasses import dataclass
from typing import Any, Union

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.wallets import WalletRepository
from crypto_overview.ui.components.empty_state import QListWidgetWithEmptyState
from crypto_overview.ui.components.wallet_forms.custom_wallet_form import CustomWalletForm
from crypto_overview.ui.components.wallet_forms.form_factory import create_wallet_form
from crypto_overview.wallet.wallet_type import WalletType

logger = logging.getLogger()


@dataclass
class WalletData:
    """
    Contains the temporary data of a wallet during the edition.
    """

    name: str
    type: str
    data: list[Any]


class WalletsDialog(QtWidgets.QDialog):
    def __init__(self, parent, wallet_repository: WalletRepository, asset_repository: AssetRepository) -> None:
        super().__init__(parent, Qt.WindowCloseButtonHint)
        self.setWindowTitle("Wallets")

        self.wallet_repository = wallet_repository
        self.asset_repository = asset_repository
        self.settings = QtCore.QSettings()

        # Dialog buttons
        self.button_box = QtWidgets.QDialogButtonBox(self)
        self.button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Save)
        self.button_box.accepted.connect(self.save_and_accept)
        self.button_box.rejected.connect(self.reject)

        # Main layout
        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.main_layout)

        self.main_hbox = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.main_hbox)
        self.main_layout.addWidget(self.button_box)

        self.left_vbox = QtWidgets.QVBoxLayout()
        self.main_hbox.addLayout(self.left_vbox, 1)

        self.right_vbox = QtWidgets.QVBoxLayout()
        self.main_hbox.addLayout(self.right_vbox, 2)

        # List of wallets
        self.wallet_list = QListWidgetWithEmptyState("No Wallets")
        self.left_vbox.addWidget(self.wallet_list)
        self.wallet_list.currentItemChanged.connect(self.change)

        for wallet in self.wallet_repository.get_wallets():
            item = QtWidgets.QListWidgetItem()
            name = wallet.get_name()
            wallet_type = WalletType.get_id_from_class(wallet)
            item.setText(wallet.get_name())
            wallet_arg_data = [
                getattr(wallet, arg_name) for arg_name, arg_type in wallet.get_required_parameters_types().items()
            ]
            wallet_data = WalletData(name, wallet_type, wallet_arg_data)
            item.setData(QtCore.Qt.UserRole, wallet_data)
            self.wallet_list.addItem(item)

        # Buttons for wallets
        wallets_button_box = QtWidgets.QHBoxLayout()
        self.left_vbox.addLayout(wallets_button_box)

        add_button = QtWidgets.QPushButton("New wallet")
        add_button.clicked.connect(self.add)
        wallets_button_box.addWidget(add_button)
        self.delete_button = QtWidgets.QPushButton("Delete")
        self.delete_button.clicked.connect(self.delete)
        wallets_button_box.addWidget(self.delete_button)
        wallets_button_box.addStretch()

        # Type of wallet
        wallet_type_form = QtWidgets.QFormLayout()
        self.right_vbox.addLayout(wallet_type_form)

        self.wallet_type_combo = QtWidgets.QComboBox()
        wallet_type_form.addRow("Type", self.wallet_type_combo)
        self.wallet_type_combo.currentIndexChanged.connect(self.wallet_type_changed)
        wallet_type_names = [w.friendly_name for w in WalletType]
        self.wallet_type_combo.addItems(wallet_type_names)

        self.wallet_id_to_combo_index = {w.id: i for i, w in enumerate(WalletType)}
        self.combo_index_to_wallet_id = {i: w.id for i, w in enumerate(WalletType)}

        # Line separator
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.right_vbox.addWidget(line)

        # Form
        self.forms_stacked_widget = QtWidgets.QStackedWidget()
        self.right_vbox.addWidget(self.forms_stacked_widget)

        self.forms: list[list[Union[QtWidgets.QLineEdit, CustomWalletForm]]] = []
        for wallet_type in WalletType:
            cls = wallet_type.cls
            params_friendly_names = cls.get_required_parameters_friendly_name()
            params_form_classes = cls.get_required_parameters_form_class()

            page = QtWidgets.QWidget()
            self.forms_stacked_widget.addWidget(page)
            form_layout = QtWidgets.QFormLayout()
            form_layout.setContentsMargins(0, 0, 0, 0)
            page.setLayout(form_layout)

            if hasattr(cls, "description"):
                description = getattr(cls, "description")
                if isinstance(description, str) and len(description) > 0:
                    description_label = QtWidgets.QLabel(description)
                    description_label.setWordWrap(True)
                    form_layout.addRow(description_label)

            controls = []
            for i, (arg_name, arg_friendly_name) in enumerate(params_friendly_names.items()):
                form_class = params_form_classes.get(arg_name, None)

                if form_class is not None:
                    form = create_wallet_form(form_class, self.asset_repository)
                    if form.add_label_to_row:
                        form_layout.addRow(arg_friendly_name, form)
                    else:
                        form_layout.addRow(form)
                    controls.append(form)
                else:
                    line_edit = QtWidgets.QLineEdit()
                    controls.append(line_edit)

                    if i == 0 and arg_name == "name":
                        line_edit.textEdited.connect(self.name_changed)

                    form_layout.addRow(arg_friendly_name, line_edit)

            self.forms.append(controls)

        # change page
        self.wallet_type_combo.activated[int].connect(self.forms_stacked_widget.setCurrentIndex)
        self.wallet_type_combo.currentIndexChanged[int].connect(self.forms_stacked_widget.setCurrentIndex)

        # Initialization
        self.wallet_list.setCurrentRow(0)
        if self.wallet_list.count() == 0:
            self.disable_form()

    def form_to_item_data(self, item: QtWidgets.QListWidgetItem):
        data = []

        for control in self.forms[self.wallet_type_combo.currentIndex()]:
            if isinstance(control, CustomWalletForm):
                data.append(control.get_data())
            else:
                data.append(control.text())

        item_data = item.data(QtCore.Qt.UserRole)
        item_data.name = self.forms[self.wallet_type_combo.currentIndex()][0].text()
        item_data.type = self.combo_index_to_wallet_id[self.wallet_type_combo.currentIndex()]
        item_data.data = data

    def item_data_to_form(self, data):
        for control, value in zip(self.forms[self.wallet_type_combo.currentIndex()], data):
            if isinstance(control, CustomWalletForm):
                control.set_data(value)
            else:
                control.setText(value)

    def clear_forms(self):
        for form in self.forms:
            for control in form:
                if isinstance(control, CustomWalletForm):
                    control.clear()
                else:
                    control.setText("")

    def wallet_type_changed(self, new_wallet_type):
        # Copy name from previous form to the new one
        if hasattr(self, "previous_wallet_type") and self.previous_wallet_type is not None:
            name_control = self.forms[new_wallet_type][0]
            previous_name_control = self.forms[self.previous_wallet_type][0]
            if not isinstance(name_control, QtWidgets.QLineEdit) or not isinstance(name_control, QtWidgets.QLineEdit):
                error = AttributeError("Invalid form, the first control must be a QLineEdit for the name argument.")
                logging.fatal(error, exc_info=True, stack_info=True)
                raise error
            name_control.setText(previous_name_control.text())
        self.previous_wallet_type = new_wallet_type

    def save_and_accept(self):
        self.form_to_item_data(self.wallet_list.currentItem())

        wallets: list[dict] = []
        for i in range(self.wallet_list.count()):
            item = self.wallet_list.item(i)
            wallet_data: WalletData = item.data(QtCore.Qt.UserRole)

            wallet_type = WalletType(wallet_data.type)
            wallet_class = wallet_type.cls

            parameters_names = wallet_class.get_required_parameters()
            parameters = dict(zip(parameters_names, wallet_data.data))

            parameters["type"] = wallet_data.type

            wallets.append(parameters)

        self.settings.setValue("wallets/wallets", wallets)
        self.settings.sync()

        self.accept()

    def delete(self):
        current_row = self.wallet_list.currentRow()
        if self.wallet_list.count() == 1:
            pass
        elif current_row == 0:
            self.wallet_list.setCurrentRow(1)
        else:
            self.wallet_list.setCurrentRow(current_row - 1)

        self.wallet_list.takeItem(current_row)

    def add(self):
        self.wallet_list.setCurrentRow(-1)
        self.wallet_type_combo.setCurrentIndex(0)
        new_name = "New wallet"
        item = QtWidgets.QListWidgetItem(new_name)

        data = []
        for control in self.forms[self.wallet_type_combo.currentIndex()]:
            if isinstance(control, CustomWalletForm):
                data.append(control.get_default_data())
            else:
                data.append("")
        data[0] = new_name
        wallet_data = WalletData(new_name, self.combo_index_to_wallet_id[self.wallet_type_combo.currentIndex()], data)
        item.setData(QtCore.Qt.UserRole, wallet_data)
        self.wallet_list.addItem(item)
        self.wallet_list.setCurrentItem(item)

    def name_changed(self, text):
        self.wallet_list.currentItem().setText(text)

    def disable_form(self, disable: bool = True):
        self.wallet_type_combo.setEnabled(not disable)
        for form in self.forms:
            for control in form:
                control.setEnabled(not disable)

    def change(self, item: QtWidgets.QListWidgetItem, previous_item: QtWidgets.QListWidgetItem):
        if previous_item is not None:
            self.form_to_item_data(previous_item)

        self.clear_forms()

        if item is None:
            self.disable_form()
            self.delete_button.setEnabled(False)
            return

        self.disable_form(False)
        self.delete_button.setEnabled(True)

        data: WalletData = item.data(QtCore.Qt.UserRole)
        self.wallet_type_combo.setCurrentIndex(self.wallet_id_to_combo_index[data.type])
        self.item_data_to_form(data.data)
