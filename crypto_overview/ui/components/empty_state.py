from typing import Optional

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt


class QTableViewWithEmptyState(QtWidgets.QTableView):
    def __init__(
        self, empty_text: str = "No Items", empty_text_size: int = 10, parent: Optional[QtCore.QObject] = None
    ):
        super().__init__(parent)
        self.empty_text = empty_text
        self.empty_text_size = empty_text_size

    def paintEvent(self, e: QtGui.QPaintEvent) -> None:
        super().paintEvent(e)
        if not self.model() or self.model().rowCount(self.rootIndex()) == 0:
            painter = QtGui.QPainter(self.viewport())
            set_painter_font_size(painter, self.empty_text_size)
            painter.drawText(self.rect(), Qt.AlignCenter, self.empty_text)


class QListWidgetWithEmptyState(QtWidgets.QListWidget):
    def __init__(
        self, empty_text: str = "No Items", empty_text_size: int = 10, parent: Optional[QtCore.QObject] = None
    ):
        super().__init__(parent)
        self.empty_text = empty_text
        self.empty_text_size = empty_text_size

    def paintEvent(self, e: QtGui.QPaintEvent) -> None:
        super().paintEvent(e)
        if not self.model() or self.model().rowCount(self.rootIndex()) == 0:
            painter = QtGui.QPainter(self.viewport())
            set_painter_font_size(painter, self.empty_text_size)
            painter.drawText(self.rect(), Qt.AlignCenter, self.empty_text)


def set_painter_font_size(painter: QtGui.QPainter, font_size: int = 10):
    font = painter.font()
    font.setPointSize(font_size)
    painter.setFont(font)
