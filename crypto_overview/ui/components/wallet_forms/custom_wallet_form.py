import abc
from typing import Any, Optional

from PyQt5 import QtCore, QtWidgets


class CustomWalletForm(QtWidgets.QWidget):
    __metaclass__ = abc.ABCMeta

    add_label_to_row: bool = True

    def __init__(self, parent: Optional[QtCore.QObject] = None):
        super().__init__(parent)

    @abc.abstractmethod
    def set_data(self, data: Any):
        pass

    @abc.abstractmethod
    def get_data(self) -> Any:
        pass

    @abc.abstractmethod
    def clear(self):
        pass

    @abc.abstractmethod
    def get_default_data(self) -> Any:
        pass
