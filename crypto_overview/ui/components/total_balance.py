from decimal import Decimal
from typing import Optional

from PyQt5 import QtCore, QtWidgets

from crypto_overview.models.price import PriceRepository


class TotalBalance(QtWidgets.QFrame):
    def __init__(self, price_repository: PriceRepository, parent: Optional[QtCore.QObject] = None):
        super().__init__(parent)

        self.price_repository = price_repository

        main_layout = QtWidgets.QVBoxLayout()
        self.setLayout(main_layout)
        self.setFrameStyle(QtWidgets.QFrame.Box | QtWidgets.QFrame.Sunken)

        # Label
        text_label = QtWidgets.QLabel("Total Balance")

        # Value
        self.value_label = QtWidgets.QLabel("...")
        font = self.value_label.font()
        font.setPointSize(18)
        self.value_label.setFont(font)

        main_layout.addWidget(text_label)
        main_layout.addWidget(self.value_label)

    def set_value(self, value: Decimal):
        symbol = self.price_repository.currency.ticker_short

        string_value = f"{value:,.2f} {symbol}"
        self.value_label.setText(string_value)
