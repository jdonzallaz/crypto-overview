import webbrowser
from decimal import Decimal

import qtawesome as qta
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QCheckBox, QComboBox, QHBoxLayout, QMainWindow, QPushButton, QVBoxLayout, QWidget

from crypto_overview import constants
from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.currency import Currency
from crypto_overview.models.price import PriceRepository
from crypto_overview.models.wallets import WalletRepository
from crypto_overview.ui.components.empty_state import QTableViewWithEmptyState
from crypto_overview.ui.components.tables import (
    AnonymizerProxyModel,
    DustSortFilterProxyModel,
    FormatPercentageProxyModel,
    RoundFloatDelegate,
    TableModel,
)
from crypto_overview.ui.components.total_balance import TotalBalance
from crypto_overview.ui.dialogs.about import AboutDialog
from crypto_overview.ui.dialogs.export import ExportDialog
from crypto_overview.ui.dialogs.settings import SettingsDialog
from crypto_overview.ui.dialogs.wallets import WalletsDialog


class MainWindow(QMainWindow):
    def __init__(
        self,
        wallet_repository: WalletRepository,
        asset_repository: AssetRepository,
        price_repository: PriceRepository,
        parent=None,
    ):
        super().__init__(parent)
        self.settings = QtCore.QSettings()
        self.wallet_repository = wallet_repository
        self.asset_repository = asset_repository
        self.price_repository = price_repository

        # Init window
        self.setWindowTitle(constants.NAME)
        self.create_menu()
        self.statusBar().show()

        # Window position
        self.move(self.settings.value("window/pos"))
        self.resize(self.settings.value("window/size"))
        if self.settings.value("window/fullscreen"):
            self.showMaximized()

        # Main layout
        main_widget = QWidget()
        main_layout = QVBoxLayout()
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)

        # Top widgets
        top_widgets_hbox = QtWidgets.QHBoxLayout()
        self.total_balance = TotalBalance(self.price_repository)
        top_widgets_hbox.addWidget(self.total_balance)
        top_widgets_hbox.addStretch()
        main_layout.addLayout(top_widgets_hbox)
        self.wallet_repository.add_observer(self.update_total_balance)
        self.update_total_balance()

        # Controls and buttons bar
        buttons_layout = self.create_controls()
        main_layout.addLayout(buttons_layout)

        # Table and model
        self.table = self.create_table()
        self.resize_table_columns_rows()
        main_layout.addWidget(self.table)

    def update_total_balance(self):
        balances = self.wallet_repository.get_balances_per_asset_per_wallet()
        balance = sum((b.value for b in balances))
        self.total_balance.set_value(balance)

    def closeEvent(self, e):
        # Overridden method, save window positioning on close
        self.settings.setValue("window/pos", self.pos())
        self.settings.setValue("window/size", self.size())
        self.settings.setValue("window/fullscreen", int(self.isMaximized()))

    def create_menu(self):
        self.menu_file = self.menuBar().addMenu("&File")
        self.menu_file.addAction("&Wallets", self.open_wallets, "alt+w")
        self.menu_file.addAction("&Export", self.export, "alt+e")
        self.menu_file.addAction("&Settings", self.open_settings, "alt+s")
        self.menu_file.addSeparator()
        self.menu_file.addAction("&Exit", self.close, "alt+f4")

        self.menu_about = self.menuBar().addMenu("&Help")
        self.menu_about.addAction("&About", self.open_about, "f1")
        self.menu_about.addAction("&Submit a bug report...", self.open_issues)

    def create_controls(self) -> QtWidgets.QLayout:
        buttons_layout = QHBoxLayout()

        # Refresh
        refresh_button = QPushButton(icon=qta.icon("ph.arrows-clockwise"), text=" Refresh")
        refresh_button.clicked.connect(self.refresh)

        # Hide balance
        hide_balance_button = QCheckBox(text="Hide Balance")
        hide_balance_value = self.settings.value("state/hide_balance")
        hide_balance_button.setChecked(hide_balance_value)
        hide_balance_button.toggled.connect(self.toggle_hide_balance)

        # Hide dust
        hide_dust_button = QCheckBox(text="Hide Dust")
        hide_dust_value = self.settings.value("state/hide_dust")
        hide_dust_button.setChecked(hide_dust_value)
        hide_dust_button.toggled.connect(self.toggle_hide_dust)

        # Currency choice
        self.currency_choice = QComboBox()
        current_currency = self.settings.value("state/currency")
        current_index = 0
        for i, currency in enumerate(Currency):
            self.currency_choice.addItem(f"{currency.friendly_name} [{currency.ticker_short}]", currency.value)
            if currency.id == current_currency:
                current_index = i
        self.currency_choice.setCurrentIndex(current_index)
        self.currency_choice.currentIndexChanged.connect(self.change_currency)

        # Add controls
        buttons_layout.addWidget(refresh_button)
        buttons_layout.addStretch()
        buttons_layout.addWidget(hide_balance_button)
        buttons_layout.addWidget(hide_dust_button)
        buttons_layout.addWidget(self.currency_choice)

        return buttons_layout

    def create_table(self) -> QtWidgets.QTableView:
        self.model = TableModel(self.wallet_repository, self.price_repository)

        hide_dust_value = self.settings.value("state/hide_dust")
        dust_threshold_value = Decimal(self.settings.value("settings/dust_threshold"))
        self.sort_filter_proxy = DustSortFilterProxyModel(5, hide_dust_value, dust_threshold_value)
        self.sort_filter_proxy.setSourceModel(self.model)

        format_percentage_proxy = FormatPercentageProxyModel({4, 6})
        format_percentage_proxy.setSourceModel(self.sort_filter_proxy)

        hide_balance_value = self.settings.value("state/hide_balance")
        self.anon_proxy = AnonymizerProxyModel(hide_balance_value)
        self.anon_proxy.setSourceModel(format_percentage_proxy)

        item_delegate = RoundFloatDelegate()

        table = QTableViewWithEmptyState("No balances\n\nGet started by adding a wallet in [File → Wallets]", 12)
        table.setItemDelegate(item_delegate)
        table.setModel(self.anon_proxy)
        table.setSortingEnabled(True)
        table.sortByColumn(1, Qt.DescendingOrder)
        table.verticalHeader().hide()
        table.setCornerButtonEnabled(False)
        table.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        table.setFocusPolicy(Qt.NoFocus)

        return table

    def resize_table_columns_rows(self):
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()

    def refresh(self):
        self.price_repository.update()
        self.wallet_repository.emit()
        self.statusBar().showMessage("Balances and prices have been refreshed", 5000)

    def toggle_hide_dust(self, enabled):
        self.sort_filter_proxy.set_hide_dust(enabled)
        self.settings.setValue("state/hide_dust", int(enabled))
        self.resize_table_columns_rows()

    def toggle_hide_balance(self, enabled):
        self.anon_proxy.set_hide_balance(enabled)
        self.settings.setValue("state/hide_balance", int(enabled))

    def change_currency(self):
        currency_str = self.currency_choice.currentData()
        currency = Currency(currency_str)
        self.price_repository.set_currency(currency)
        self.price_repository.update()
        self.settings.setValue("state/currency", currency.id)

    def export(self):
        if ExportDialog(self, self.wallet_repository, self.price_repository).exec():
            self.statusBar().showMessage("Export successful", 5000)

    def open_wallets(self):
        dialog = WalletsDialog(self, self.wallet_repository, self.asset_repository)
        if dialog.exec() == QtWidgets.QDialog.Accepted:
            self.wallet_repository.load()
            self.wallet_repository.refresh_prices()
            self.resize_table_columns_rows()

    def open_settings(self):
        dialog = SettingsDialog(self, self.asset_repository)
        nb_coins_value = self.settings.value("settings/nb_coins")
        if dialog.exec() == QtWidgets.QDialog.Accepted:
            if self.settings.value("settings/nb_coins") != nb_coins_value:
                self.asset_repository.update()
            self.sort_filter_proxy.set_dust_threshold(Decimal(self.settings.value("settings/dust_threshold")))
            self.price_repository.update_periodic_update_interval()
            self.resize_table_columns_rows()

    def open_about(self):
        dialog = AboutDialog(self)
        dialog.exec()

    def open_issues(self):
        webbrowser.open(f"{constants.WEBSITE_LINK}/issues")
