from enum import Enum
from typing import Optional


class Currency(Enum):
    AUD = "aud", "Australian Dollar", "AUD", "$"
    BRL = "brl", "Brazilian real", "BRL", "R$"
    CAD = "cad", "Canadian dollar", "CAD", "$"
    CHF = "chf", "Swiss franc", "CHF"
    CNY = "cny", "Renminbi", "CNY"
    CZK = "czk", "Czech crown", "CZK", "Kč"
    DKK = "dkk", "Danish crown", "DKK", "kr."
    EUR = "eur", "Euro", "EUR", "€"
    GBP = "gbp", "British pound", "GBP", "£"
    HKD = "hkd", "Hong Kong dollar", "HKD", "$"
    HUF = "huf", "Forint", "HUF", "Ft"
    IDR = "idr", "Rupiah", "IDR", "Rp"
    ILS = "ils", "Israeli shekel", "ILS", "₪"
    INR = "inr", "Indian rupee", "INR", "₹"
    JPY = "jpy", "Yen", "JPY", "¥"
    KRW = "krw", "South Korean won", "KRW", "₩"
    MXN = "mxn", "Mexican peso", "MXN", "$"
    MYR = "myr", "Malaysian ringgit", "MYR", "RM"
    NOK = "nok", "Norwegian crown", "NOK", "kr"
    NZD = "nzd", "New Zealand dollar", "NZD", "$"
    PHP = "php", "Philippine peso", "PHP", "₱"
    PLN = "pln", "Polish zloty", "PLN", "zł"
    RUB = "rub", "Russian ruble", "RUB", "₽"
    SEK = "sek", "Swedish crown", "SEK", "kr"
    SGD = "sgd", "Singapore dollar", "SGD", "$"
    THB = "thb", "Thai baht", "THB", "฿"
    TRY = "try", "Turkish lira", "TRY", "₺"
    USD = "usd", "US dollar", "USD", "$"
    ZAR = "zar", "Rand", "ZAR", "R"

    def __new__(cls, id: str, friendly_name: str, ticker: str, ticker_short: Optional[str] = None):
        obj = object.__new__(cls)
        obj._value_ = id
        obj.id = id
        obj.friendly_name = friendly_name
        obj.ticker = ticker
        obj.ticker_short = ticker if ticker_short is None else ticker_short
        return obj
