import math
from dataclasses import dataclass
from enum import Enum

import diskcache
from PyQt5 import QtCore
from pycoingecko import CoinGeckoAPI

from crypto_overview.utils.cache import cache_method
from crypto_overview.utils.observer import Observable
from crypto_overview.utils.periodic_update import PeriodicUpdate
from crypto_overview.models.currency import Currency


class AssetType(Enum):
    CURRENCY = "currency"
    CRYPTO_CURRENCY = "crypto-currency"


@dataclass(frozen=True, eq=True)
class Asset:
    id: str
    symbol: str
    name: str
    type: AssetType


def create_unknown_asset(symbol: str = "UNKNOWN"):
    return Asset(id="unknown", symbol=symbol, name=f"{symbol} (unknown)", type=AssetType.CRYPTO_CURRENCY)


def create_currency_assets() -> set[Asset]:
    return set((Asset(c.id, c.ticker, c.friendly_name, AssetType.CURRENCY) for c in Currency))


class AssetRepository(Observable, PeriodicUpdate):
    """
    Repository class for assets. Fetch, index and retrieve assets data.
    Fetched assets data is cached for one day.
    """

    def __init__(self, cache: diskcache.Cache):
        self.cache = cache

        self.settings = QtCore.QSettings()
        self.coingecko = CoinGeckoAPI()

        self.assets: list[Asset] = []
        self.id_map: dict[str, Asset] = {}
        self.symbol_map: dict[str, Asset] = {}

        self.currencies = list(create_currency_assets())

        self.update()

        PeriodicUpdate.__init__(self, "update_frequency/assets")

    def get_by_symbol_or_id(self, symbol_or_id: str) -> Asset:
        symbol_or_id_lower = symbol_or_id.lower()

        if symbol_or_id_lower in self.symbol_map:
            return self.symbol_map[symbol_or_id_lower]

        if symbol_or_id_lower in self.id_map:
            return self.id_map[symbol_or_id_lower]

        return create_unknown_asset(symbol_or_id)

    def get_by_id(self, id_: str) -> Asset:
        id_lower = id_.lower()

        if id_lower in self.id_map:
            return self.id_map[id_lower]

        return create_unknown_asset(id_)

    def get_by_symbol(self, symbol: str) -> Asset:
        symbol_lower = symbol.lower()

        if symbol_lower in self.symbol_map:
            return self.symbol_map[symbol_lower]

        return create_unknown_asset(symbol)

    def get_assets(self, sort_by_name: bool = False) -> list[Asset]:
        if sort_by_name:
            return sorted(self.assets, key=lambda a: a.name)

        return self.assets

    @cache_method("assets", expire=86400)
    def fetch(self, n_coins: int, *, ignore_cache: bool = False) -> list[Asset]:
        assets_data = []
        n_page = int(math.ceil(n_coins / 250))
        for page in range(1, n_page + 1):
            assets_data += self.coingecko.get_coins_markets(
                vs_currency="usd", order="market_cap_desc", per_page=250, page=page
            )

        assets = [
            Asset(id=asset["id"], symbol=asset["symbol"], name=asset["name"], type=AssetType.CRYPTO_CURRENCY)
            for asset in assets_data
        ]

        return assets

    def update(self, ignore_cache: bool = False):
        self.assets = self.fetch(self.settings.value("settings/nb_coins"), ignore_cache=ignore_cache)
        self.index_assets()
        self.emit()

    def index_assets(self):
        self.id_map = {}
        self.symbol_map = {}

        for asset in self.currencies + self.assets:
            # Keep only the first asset for each key. This assumes the assets list is ordered by market cap, and thus
            # the get_by method retrieves only the coin with the highest market cap, if multiple coins have the same key
            self.id_map.setdefault(asset.id.lower(), asset)
            self.symbol_map.setdefault(asset.symbol.lower(), asset)

    def periodic_update(self):
        self.update(ignore_cache=True)
