from __future__ import annotations

from dataclasses import dataclass
from decimal import Decimal

from crypto_overview.models.asset import Asset


@dataclass
class Balance:
    asset: Asset
    free: Decimal
    locked: Decimal
    rewards: Decimal

    @property
    def total(self) -> Decimal:
        return self.free + self.locked + self.rewards

    def __post_init__(self):
        self.normalize()

    def normalize(self):
        self.free = self.free.normalize()
        self.locked = self.locked.normalize()
        self.rewards = self.rewards.normalize()

    def __add__(self, other: Balance) -> Balance:
        return Balance(self.asset, self.free + other.free, self.locked + other.locked, self.rewards + other.rewards)


@dataclass
class PricedBalance(Balance):
    price: Decimal
    change_24h: Decimal

    def __post_init__(self):
        self.price = self.price.normalize()
        self.change_24h = self.change_24h.normalize()

    @property
    def value(self) -> Decimal:
        return self.total * self.price

    @classmethod
    def from_balance(cls, balance: Balance, **kwargs):
        return cls(**balance.__dict__, **kwargs)


@dataclass
class WalletAssetPricedBalance(PricedBalance):
    wallet: "Wallet"
    share: Decimal

    @classmethod
    def from_balance(cls, balance: Balance, **kwargs):
        return cls(**balance.__dict__, **kwargs)


@dataclass
class AssetBalance:
    asset: Asset
    balances: list[Balance]
    price: Decimal
    change_24h: Decimal
    share: Decimal

    def __post_init__(self):
        self.price = self.price.normalize()
        self.change_24h = self.change_24h.normalize()
        self.share = self.share.normalize()

    @property
    def value(self) -> Decimal:
        return sum([b.total for b in self.balances]) * self.price


@dataclass
class WalletBalance:
    wallet: "Wallet"
    balances: list[PricedBalance]
    share: Decimal

    @property
    def value(self) -> Decimal:
        return sum([b.value for b in self.balances])

    @property
    def assets(self) -> set[Asset]:
        return set([b.asset for b in self.balances])
