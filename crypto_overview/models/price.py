import logging
from decimal import Decimal
from typing import Iterable, Optional

import diskcache
import requests
from PyQt5 import QtCore
from pycoingecko import CoinGeckoAPI

from crypto_overview.models.asset import Asset, AssetType
from crypto_overview.utils.cache import cache_method
from crypto_overview.utils.numbers_ import float_to_decimal
from crypto_overview.utils.observer import Observable
from crypto_overview.utils.periodic_update import PeriodicUpdate
from crypto_overview.models.currency import Currency

CURRENCY_API = "https://www.currency-api.com"

logger = logging.getLogger()


class PriceRepository(Observable, PeriodicUpdate):
    """
    Repository class for prices. Fetch, index and retrieve prices data.
    The available data is the price and the 24h change in percent for the given assets.
    Fetched price data is cached for 5 seconds.
    Also retrieve currency (usd, chf, ...) rates, without 24h change. Those rates are cached for 1 hour.
    """

    currency_api: str = CURRENCY_API

    def __init__(
        self, cache: diskcache.Cache, currency: Currency = Currency.USD, assets: Optional[Iterable[Asset]] = None
    ):
        self.cache = cache
        self.currency = currency
        if assets is not None:
            self.add_assets(assets)

        self.settings = QtCore.QSettings()
        self.coingecko = CoinGeckoAPI()

        self.assets: set[Asset] = set()
        self.prices: dict[str, Decimal] = {}
        self.change_24h: dict[str, Decimal] = {}

        self.update()

        PeriodicUpdate.__init__(self, "update_frequency/prices")

    def __getitem__(self, asset: Asset) -> Decimal:
        return self.get_price(asset)

    def get_price(self, asset: Asset) -> Decimal:
        if asset.id == "unknown" or asset.id not in self.prices:
            return Decimal(0)

        return self.prices[asset.id]

    def get_24h_change(self, asset: Asset) -> Decimal:
        if asset.id == "unknown" or asset.id not in self.change_24h:
            return Decimal(0)

        return self.change_24h[asset.id]

    def get_supported_currencies(self) -> list[str]:
        return self.coingecko.get_supported_vs_currencies()

    def add_assets(self, assets: Iterable[Asset]):
        assets = (asset for asset in assets if asset.id != "unknown")
        self.assets.update(assets)

    def set_currency(self, currency: Currency):
        self.currency = currency

    @cache_method("prices_currencies", expire=3600)
    def fetch_currencies(
        self, assets_ids: list[str], currency_id: str, *, ignore_cache: bool = False
    ) -> dict[str, float]:
        if len(assets_ids) == 0:
            return {}

        symbols = ",".join((a.upper() for a in assets_ids))
        response = requests.get(f"{self.currency_api}/rates?base={currency_id.upper()}&symbols={symbols}")

        if response.status_code != 200:
            msg = "Currency price request failed due to an unknown error."
            logger.error(msg)
            return {}

        data = response.json()

        return data["rates"]

    @cache_method("prices_crypto", expire=5)
    def fetch_crypto(
        self, assets_ids: list[str], currency_id: str, *, ignore_cache: bool = False
    ) -> dict[str, dict[str, float]]:
        if len(assets_ids) == 0:
            return {}

        return self.coingecko.get_price(assets_ids, currency_id, include_24hr_change=True)

    def update(self, assets: Optional[Iterable[Asset]] = None):
        if assets is not None:
            self.add_assets(assets)

        # Crypto currencies prices
        crypto_assets_ids = [asset.id for asset in self.assets if asset.type is AssetType.CRYPTO_CURRENCY]
        crypto_prices_data = self.fetch_crypto(crypto_assets_ids, self.currency.id)

        self.prices = {
            k: float_to_decimal(v[self.currency.id]) for k, v in crypto_prices_data.items() if self.currency.id in v
        }

        change_24h_key = f"{self.currency.id}_24h_change"
        self.change_24h = {
            k: float_to_decimal(v[change_24h_key]) for k, v in crypto_prices_data.items() if change_24h_key in v
        }

        # Currencies prices
        currency_assets_ids = [asset.id for asset in self.assets if asset.type is AssetType.CURRENCY]
        currency_prices_data = self.fetch_currencies(currency_assets_ids, self.currency.id)
        self.prices.update({k.lower(): 1 / float_to_decimal(v) for k, v in currency_prices_data.items()})

        self.emit()

    def periodic_update(self):
        self.update()
