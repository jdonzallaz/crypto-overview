import logging
from decimal import Decimal
from typing import Union

from PyQt5.QtCore import QSettings

from crypto_overview.models.asset import Asset, AssetRepository
from crypto_overview.models.balance import AssetBalance, PricedBalance, WalletAssetPricedBalance, WalletBalance
from crypto_overview.models.price import PriceRepository
from crypto_overview.utils.observer import Observable
from crypto_overview.utils.periodic_update import PeriodicUpdate
from crypto_overview.wallet.wallet import Wallet
from crypto_overview.wallet.wallet_type import WalletType

logger = logging.getLogger()


class WalletRepository(Observable, PeriodicUpdate):
    """
    Repository class for wallets and balances. Load wallets, get balances and group the data,
    together with prices, 24h-change, etc...
    """

    def __init__(self, asset_repository: AssetRepository, price_repository: PriceRepository):
        self.asset_repository = asset_repository
        self.asset_repository.add_observer(self.on_asset_update)
        self.price_repository = price_repository
        self.price_repository.add_observer(self.on_price_update)
        self.settings = QSettings()

        self.wallets: list[Wallet] = []

        self.load()
        self.refresh_prices()

        PeriodicUpdate.__init__(self, "update_frequency/balances")

    def on_asset_update(self):
        self.refresh_prices()
        self.refresh_wallets()

    def on_price_update(self):
        self.emit()

    def refresh_wallets(self):
        self.load()

    def refresh_prices(self):
        self.price_repository.update(self.get_assets())

    def periodic_update(self):
        # For now, there is no cache, the data is loaded from wallets on each call to get_balance_*** and
        # the balance classes are recomputed. All we have to do is emit(), and the observers will get balances directly.
        self.emit()

    def load(self):
        wallets_configs = self.settings.value("wallets/wallets")
        wallets = []
        for config in wallets_configs:
            type = config["type"]
            wallet_type = WalletType(type)
            wallet_class = wallet_type.cls

            required_parameters = wallet_class.get_required_parameters()
            parameters = {k: config[k] for k in required_parameters if k in config}
            if len(parameters) < len(required_parameters):
                continue

            wallet = wallet_class(self.asset_repository, **parameters)
            wallets.append(wallet)

        self.wallets = wallets

    def get_wallets(self) -> list[Wallet]:
        return self.wallets

    def get_assets(self) -> set[Asset]:
        wallets = self.get_wallets()

        assets = set()
        for w in wallets:
            wallet_balances = w.get_balances()
            for b in wallet_balances:
                assets.add(b.asset)

        return assets

    def get_balances_per_asset_per_wallet(self) -> list[WalletAssetPricedBalance]:
        wallets = self.get_wallets()

        balances = []
        for wallet in wallets:
            wallet_balances = wallet.get_balances()
            for b in wallet_balances:
                balance = WalletAssetPricedBalance.from_balance(
                    balance=b,
                    wallet=wallet,
                    price=self.price_repository.get_price(b.asset),
                    change_24h=self.price_repository.get_24h_change(b.asset),
                    share=Decimal(0),
                )
                balances.append(balance)

        self._add_share(balances)

        logger.info(f"get_balances_per_asset_per_wallet returned {len(balances)} balances")

        return balances

    def get_balances_per_asset(self) -> list[AssetBalance]:
        wallets = self.get_wallets()

        balances_dict: dict[Asset, AssetBalance] = {}
        for wallet in wallets:
            wallet_balances = wallet.get_balances()
            for balance in wallet_balances:
                asset = balance.asset
                if asset in balances_dict:
                    balances_dict[asset].balances.append(balance)
                else:
                    balances_dict[asset] = AssetBalance(
                        asset=asset,
                        balances=[balance],
                        price=self.price_repository.get_price(balance.asset),
                        change_24h=self.price_repository.get_24h_change(balance.asset),
                        share=Decimal(0),
                    )

        balances = list(balances_dict.values())

        self._add_share(balances)

        return balances

    def get_balances_per_wallet(self) -> list[WalletBalance]:
        wallets = self.get_wallets()

        balances = []
        for wallet in wallets:
            wallet_balances = wallet.get_balances()
            priced_balances = []
            for b in wallet_balances:
                balance = PricedBalance.from_balance(
                    balance=b,
                    price=self.price_repository.get_price(b.asset),
                    change_24h=self.price_repository.get_24h_change(b.asset),
                )
                priced_balances.append(balance)
            balance = WalletBalance(
                wallet=wallet,
                balances=priced_balances,
                share=Decimal(0),
            )
            balances.append(balance)

        self._add_share(balances)

        return balances

    @staticmethod
    def _add_share(balances: list[Union[WalletAssetPricedBalance, AssetBalance, WalletBalance]]):
        total_value = sum([b.value for b in balances])

        for b in balances:
            if total_value > 0:
                b.share = b.value / total_value
            else:
                b.share = Decimal(0)
