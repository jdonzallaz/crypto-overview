import logging
from decimal import Decimal
from typing import Annotated

import requests

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.balance import Balance
from crypto_overview.utils.numbers_ import create_convert_decimal_unit
from crypto_overview.wallet.wallet import Wallet, WalletNameType

logger = logging.getLogger()

DEFAULT_ALGORAND_API = "https://algoindexer.algoexplorerapi.io"

AlgorandPublicAddressType = Annotated[str, "Public Address", r"([A-Z0-9]){58}"]


class AlgorandWallet(Wallet):
    api: str = DEFAULT_ALGORAND_API
    description: str = (
        f"Connect to the your wallet on the Algorand blockchain. "
        f'Get your ALGO balance using the "{DEFAULT_ALGORAND_API}" API.'
    )

    def __init__(
        self,
        asset_repository: AssetRepository,
        name: WalletNameType,
        public_address: AlgorandPublicAddressType,
    ):
        super().__init__(asset_repository, name)
        self.public_address = public_address
        self.algorand_convert_amount_raw_to_algo = create_convert_decimal_unit(6)

    def get_balances(self) -> list[Balance]:
        response = requests.get(f"{self.api}/v2/accounts/{self.public_address}")

        if response.status_code != 200:
            msg = f"Algorand accounts request failed due to an unknown error. Wallet={self.name}"
            logger.error(msg)
            return []

        data = response.json()

        free_balance = self.algorand_convert_amount_raw_to_algo(data["account"]["amount"])
        rewards_balance = self.algorand_convert_amount_raw_to_algo(data["account"]["rewards"])

        asset = self.asset_repository.get_by_symbol_or_id("algo")

        return [Balance(asset, free=free_balance, locked=Decimal(0), rewards=rewards_balance)]
