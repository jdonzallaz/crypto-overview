from decimal import Decimal
from typing import Annotated, Union

from crypto_overview.models.asset import AssetRepository
from crypto_overview.models.balance import Balance
from crypto_overview.wallet.wallet import Wallet, WalletNameType

BalancesDictType = Annotated[
    dict[str, Union[str, float, int, Decimal]], "Balances", "", "BalancesForm"
]


class CustomWallet(Wallet):
    description: str = (
        f"Manually create your wallet with custom balances. "
        f"If your exchange or blockchain is not supported, this is for you!"
    )

    def __init__(
        self,
        asset_repository: AssetRepository,
        name: WalletNameType,
        balances: BalancesDictType,
    ):
        super().__init__(asset_repository, name)
        self.balances = balances

    def get_balances(self) -> list[Balance]:
        balances = []
        for asset_name, amount in self.balances.items():
            asset = self.asset_repository.get_by_symbol_or_id(asset_name)
            balance = Balance(asset, free=Decimal(amount), locked=Decimal(0), rewards=Decimal(0))
            balances.append(balance)

        return balances
