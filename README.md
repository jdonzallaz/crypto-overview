# Crypto-Overview

Get an overview of all your crypto wallets in a single place.

## Features

- Show portfolio from various sources.
- Connect to various exchange and blockchains
    - Binance spot account
    - Nano blockchain
    - Custom wallet
- Show your balances in different currencies
- All your data is stored locally

### Work-In-Progress

- Add more exchanges and blockchains
- Make a proper release
- Add more tests
- Improve doc
- Improve internals: error catching, logging, caching
- Add table with balances grouped by asset
- Add table with balances grouped by wallet

## Getting started

Follow the *Development* information. The release will be made available shortly.

## Development

### Installation

```sh
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Python 3.9+ is required.

### Run

To run manually:

```sh
python main.py
```

### Test

Tests are run automatically in CI/CD.

To run manually:

```sh
pytest tests -v
pytest crypto_overview --doctest-modules -v
```

## Author

Crypto-Overview is developed by [Jonathan Donzallaz](mailto:jonathan.donzallaz@gmail.com).

## License

This project is licensed under the terms of the MIT license.
